import React, { Fragment,Component } from 'react';
import moment from 'moment';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Card, { CardHeader,  CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Done from '@material-ui/icons/Done';
import Close from '@material-ui/icons/Close';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import ExpandMore from '@material-ui/icons/ExpandMore';

const checkAccess = ({accessType, expires = Date.now()}) => {
  switch (accessType) {
    case 'temporal':
      return (parseInt(moment(expires).format('x'),10)>Date.now())
    case 'closed':
      return false;
    default:
      return true;
  }
}

class TsDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tsData:null
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if (nextProps.id!==prevState.id) {
      return {id:nextProps.id, tsData: null};
    };
    return null;
  }

  getTsData = (id) => {
      fetch('/ts/'+id)
        .then(response => response.json())
        .then(data => {
            this.setState({tsData:data});
        })
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.state.tsData === null)&&(this.state.id)) {
      this.getTsData(this.state.id)
    }
  }

  transferts = (id) => {
    console.log(id);
    fetch('/ts/transfer/'+id)
      .then((res) => this.getTsData(id))
      .catch((res) => console.log(res))
  }

  render() {
    let tsData = this.state.tsData;
    let access = (tsData)?checkAccess({accessType:tsData.access,expires:tsData.expires}):false;
    return (
      // <Paper style={{margin:12,padding:32,display:'flex',flexDirection:'column'}}>
      <Fragment>
        {(tsData)
          ?<Card>
            <CardHeader
              avatar={
                (access)
                  ?<Avatar style={{backgroundColor:'#4CAF50'}} aria-label="Recipe">
                    <Done />
                  </Avatar>
                  :<Avatar style={{backgroundColor:'#C62828'}} aria-label="Recipe">
                    <Close />
                  </Avatar>}
            title={`${tsData.licPlate}`}
            subheader={tsData.model}
            >

            </CardHeader>
            <CardContent>
              <Typography variant="caption" gutterBottom>{`Номер участка: ${tsData.location}`}</Typography>
              <Typography variant="caption" gutterBottom>{`Тип доступа: ${
                (
                  () => {
                    switch (tsData.access) {
                      case 'permanent':
                        return 'постоянный';
                      case 'temporal':
                        return 'временный';
                      case 'once':
                        return 'разовый';
                      case 'closed':
                        return 'закрыт';
                      default:
                        return '';
                    }
                  }
                  )()
              }`}
            </Typography>
            {(tsData.access==='temporal')?<Typography variant="caption" style={(access)?{color:'#4CAF50'}:{color:'#C62828'}} gutterBottom>{`Дата окончания доступа: ${tsData.expires}`}</Typography>:null}
            </CardContent>
            <CardActions style={{display: 'flex',justifyContent: 'space-between'}}>
              {(access)
              ?(tsData.onTerritory)
                ?<Button variant="raised" color="primary" onClick={() => this.transferts(this.state.id)}>
                  {'Фиксировать выход'}
                  <ArrowBack />
                </Button>
                :<Button variant="raised" color="primary" onClick={() => this.transferts(this.state.id)}>
                  <ArrowForward />
                  {'Фиксировать вход'}
                </Button>
              :<span></span>}
              <IconButton>
                <ExpandMore />
              </IconButton>
            </CardActions>
          </Card>
          :null}
      </Fragment>
    );
  }

}

export default TsDetails;
