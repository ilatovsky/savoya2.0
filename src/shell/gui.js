const MODE = (process.env.MODE)?process.env.MODE:'prod';
console.log(`App running in ${MODE} mode`);

const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

const backend = require('../backend');

app.on('ready', () => {
  const gui = new BrowserWindow({width: 1000, height: 1000, x:0, y:0});
  if (MODE=='dev') {
      gui.loadURL('http://localhost:3000')
  } else {
    gui.loadURL(url.format({
     pathname: path.join(__dirname,'../..','build', 'index.html'),
     protocol: 'file:',
     slashes: true
   }))
  };

})

app.on('window-all-closed', () => {
    app.quit()
})
