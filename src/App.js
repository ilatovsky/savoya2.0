import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import CssBaseline from 'material-ui/CssBaseline';
import Grid from 'material-ui/Grid';
import BrightnessHigh from '@material-ui/icons/BrightnessHigh';

import PersonForm from './PersonForm';
import PersonList from './PersonList';
import TsList from './TsList';
import PersonDetails from './PersonDetails';
import TsForm from './TsForm';
import TsDetails from './TsDetails';

import Button from 'material-ui/Button';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      personId: null,
      tsId: null,
      layout: 'persons'
    };
  }

  editPerson = (id) => {
    this.setState({...this.state,...{personId:id}});
  }

  editTs = (id) => {
    this.setState({...this.state,...{tsId:id}});
  }

  render() {
    return (
      <div className="App">
        <CssBaseline />
        <AppBar position='static'>
          <Toolbar>
            <BrightnessHigh />
            <Button color="inherit" onClick={() => this.setState({...this.state,...{layout:'persons'}})}>Люди</Button>
            <Button color="inherit" onClick={() => this.setState({...this.state,...{layout:'ts'}})}>Транспорт</Button>
          </Toolbar>
        </AppBar>

        {
          (this.state.layout==='persons')
          ?
          <Grid container>
            <Grid item xs={8}>
              <PersonList editPerson={this.editPerson}/>
            </Grid>
            <Grid item xs={4}>
              <Grid container direction={'column'}>
                <Grid item>
                  <PersonDetails id={this.state.personId} />
                  <PersonForm id={this.state.personId}/>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          :
          <Grid container>
            <Grid item xs={8}>
              <TsList editPerson={this.editTs}/>
            </Grid>
            <Grid item xs={4}>
              <Grid container direction={'column'}>
                <Grid item>
                  <TsForm id={this.state.tsId}/>
                </Grid>
                <Grid item>
                  <TsDetails id={this.state.tsId} />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        }

        {/*
        <Grid container>
          <Grid item>
            <TsDetails id={this.state.tsId} />
          </Grid>
          <Grid item>
            <TsList editTs={this.editTs}/>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item>
            <TsForm id={this.state.tsId}/>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item>

          </Grid>
        </Grid>
        <Grid container>
          <Grid item >

          </Grid>
          <Grid item>

          </Grid>
        </Grid> */}
        {/* <Drawer open>

        </Drawer> */}
      </div>
    );
  }
}

export default App;
