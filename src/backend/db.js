const fs = require('fs');
const {dbURL} = require('./config.js');
const transfers = require('./transfers.js');

const readDbFromFile = () => {
  let rawData = fs.readFileSync(dbURL)
  let db = JSON.parse(rawData);
  return db;
}

const writeDbToFile = (DB) => {
  fs.writeFileSync(dbURL,JSON.stringify(DB));
}

const DB = readDbFromFile();

const getDB = () => {
  return DB;
}

const getPersons = () => {
  return DB.persons;
}

const getTs = () => {
  console.log(DB);
  return DB.ts;
}

const dbCRUD = ({action,payload}) => {
  switch (action) {
    case 'ADD_PERSON':
      DB.persons[payload.id] = payload.data;
      writeDbToFile(DB);
      break;
    case 'READ_PERSON':
      return DB.persons[payload.id];
      break;
    case 'TRANSFER_PERSON':
      if (DB.persons[payload.id].access==='once') {
        if (DB.persons[payload.id].onTerritory){
          DB.persons[payload.id].onTerritory = false;
          DB.persons[payload.id].access = 'closed';
        } else {
          DB.persons[payload.id].onTerritory = true;
        };
      } else {
        DB.persons[payload.id].onTerritory = !DB.persons[payload.id].onTerritory;
      };
      let direction =  (DB.persons[payload.id].onTerritory)?'in':'out';
      transfers.fixTransfer({item:'person',id:payload.id,direction:direction});
      writeDbToFile(DB);
      break;
    case 'EDIT_PERSON':
      DB.persons[payload.id] = payload.data;
      writeDbToFile(DB);
      break;
    case 'DELETE_PERSON':
      delete DB.persons[payload.id];
      writeDbToFile(DB);
      break;
    case 'ADD_TS':
      DB.ts[payload.id] = payload.data;
      writeDbToFile(DB);
      break;
    case 'READ_TS':
      return DB.ts[payload.id];
      break;
    case 'EDIT_TS':
      DB.ts[payload.id] = payload.data;
      writeDbToFile(DB);
      break;
    case 'DELETE_TS':
      delete DB.ts[payload.id];
      writeDbToFile(DB);
      break;
    case 'TRANSFER_TS':{
      if (DB.ts[payload.id].access==='once') {
        if (DB.ts[payload.id].onTerritory){
          DB.ts[payload.id].onTerritory = false;
          DB.ts[payload.id].access = 'closed';
        } else {
          DB.ts[payload.id].onTerritory = true;
        };
      } else {
        DB.ts[payload.id].onTerritory = !DB.ts[payload.id].onTerritory;
      };
      let direction =  (DB.ts[payload.id].onTerritory)?'in':'out';
      transfers.fixTransfer({item:'ts',id:payload.id,direction:direction});
      writeDbToFile(DB);
      break;}
    default:
      return true;
  }
}

module.exports.dbCRUD = dbCRUD;
module.exports.getDB = getDB;
module.exports.getPersons = getPersons;
module.exports.getTs = getTs;
