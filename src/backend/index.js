const {port} = require('./config');

const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors');

const personsRouter = require('./routes/personsRouter')
const tsRouter = require('./routes/tsRouter')
const transfersRouter = require('./routes/transfersRouter')

const backend = express();
backend.use(cors());
backend.use(bodyParser.urlencoded({ extended: false }));
backend.use(bodyParser.json());

backend.use('/persons',personsRouter);
backend.use('/ts',tsRouter);
backend.use('/transfers',transfersRouter);

backend.listen(port, () => {
  console.log(`Backend running on ${port} port!`);
});
