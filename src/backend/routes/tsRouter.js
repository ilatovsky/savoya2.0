const express = require('express');
const router = express.Router();
const crypto = require('crypto');

const storage = require('../db.js');

router.get('/', function(req, res) {
  let tss = storage.getTs();
  res.send(tss);
});

router.get('/:id', function(req, res) {
  let ts = storage.dbCRUD({action:'READ_TS',payload:{id:req.params.id}});
  res.send(ts);
});

router.post('/create', function( req, res, next) {
  let data = req.body;
  let id = crypto.createHash('md5').update((Date.now().toString()+Math.random()).toString()).digest("hex");
  storage.dbCRUD({action:'ADD_TS',payload:{id:id,data:data}});
  res.send('Ts created');
});

router.post('/edit/:id', function( req, res, next) {
  let data = req.body;
  let id = req.params.id;
  storage.dbCRUD({action:'EDIT_TS',payload:{id:id,data:data}});
  res.send('Ts edited');
});

router.delete('/:id', function( req, res, next) {
  let id = req.params.id;
  storage.dbCRUD({action:'DELETE_TS',payload:{id:id}});
  res.send('Ts deleted');
});

router.get('/transfer/:id', function( req, res, next) {
  let id = req.params.id;
  storage.dbCRUD({action:'TRANSFER_TS',payload:{id:id}});
  res.send('Ts transfered');
});

module.exports = router;
