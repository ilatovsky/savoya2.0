const express = require('express');
const router = express.Router();
const crypto = require('crypto');

const storage = require('../db.js');

router.get('/', function(req, res) {
  let persons = storage.getPersons();
  res.send(persons);
});

router.get('/:id', function(req, res) {
  let person = storage.dbCRUD({action:'READ_PERSON',payload:{id:req.params.id}});
  res.send(person);
});

router.post('/create', function( req, res, next) {
  let data = req.body;
  console.log(data);
  let id = crypto.createHash('md5').update((Date.now().toString()+Math.random()).toString()).digest("hex");
  storage.dbCRUD({action:'ADD_PERSON',payload:{id:id,data:data}});
  res.send('Person created');
});

router.post('/edit/:id', function( req, res, next) {
  let data = req.body;
  let id = req.params.id;
  storage.dbCRUD({action:'EDIT_PERSON',payload:{id:id,data:data}});
  res.send('Person edited');
});

router.get('/transfer/:id', function( req, res, next) {
  let id = req.params.id;
  storage.dbCRUD({action:'TRANSFER_PERSON',payload:{id:id}});
  res.send('Person transfered');
});


router.delete('/:id', function( req, res, next) {
  let id = req.params.id;
  storage.dbCRUD({action:'DELETE_PERSON',payload:{id:id}});
  res.send('Person deleted');
});


module.exports = router;
