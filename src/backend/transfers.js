const fs = require('fs');
const {transfersURL} = require('./config.js');
const moment = require('moment');
const readDbFromFile = () => {
  let rawData = fs.readFileSync(transfersURL)
  let db = JSON.parse(rawData);
  return db;
}

const writeDbToFile = (transfersDB) => {
  fs.writeFileSync(transfersURL,JSON.stringify(transfersDB));
}

const transfersDB = readDbFromFile();

const getDB = () => {
  return transfersDB;
};

const fixTransfer = ({item,id,direction}) => {
  transfersDB.transfers.push({item,id,direction,date:moment().format('YYYY-MM-DD HH:mm')});
  writeDbToFile(transfersDB);
}

module.exports.fixTransfer = fixTransfer;
