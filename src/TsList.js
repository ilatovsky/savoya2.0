import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
// import Button from 'material-ui/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Table, {TableBody, TableCell, TableRow} from 'material-ui/Table';

class TsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ts: null
    };
    this.getTs = this.getTs.bind(this);
  }

  getTs = () => {
    fetch('/ts')
      .then(response => response.json())
      .then(data => this.setState({...this.state,...{ts:data}}));
  }

  componentDidMount() {
    this.getTs();
  }

  deleteTs = (id) => {
    fetch('/ts/'+id,{method:'DELETE'})
    .then((res) => this.getTs())
    .catch((res) => console.log(res))
  }

  render() {
    let ts = this.state.ts;
    return (
      <Paper style={{margin:12,padding:32,display:'flex',flexDirection:'column'}}>
        {(ts)
            ?<Table>
                <TableBody>
                  {Object.keys(ts).map((id) => {
                    return (
                      <TableRow key={id}>
                        {Object.keys(ts[id]).map(prop => ((prop!=='onTerritory')&&(prop!=='access'))?<TableCell key={prop} data={prop}>{ts[id][prop]}</TableCell>:null)}
                        <TableCell>
                          <EditIcon onClick={() => this.props.editTs(id)}/>
                          <DeleteIcon onClick={() => this.deleteTs(id)}/>
                        </TableCell>
                      </TableRow>)
                    })
                  }
                </TableBody>
            </Table>
            :null
        }
      </Paper>
    );
  }

}

export default TsList;
