import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
// import Button from 'material-ui/Button';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Table, {TableBody, TableCell, TableRow} from 'material-ui/Table';

class PersonList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: null
    };
    this.getPersons = this.getPersons.bind(this);
  }

  getPersons = () => {
    fetch('/persons')
      .then(response => response.json())
      .then(data => this.setState({...this.state,...{persons:data}}));
  }

  componentDidMount() {
    this.getPersons();
  }

  deletePerson = (id) => {
    fetch('/persons/'+id,{method:'DELETE'})
    .then((res) => this.getPersons())
    .catch((res) => console.log(res))
  }

  render() {
    let persons = this.state.persons;
    return (
      <Paper style={{margin:12,padding:32,display:'flex',flexDirection:'column'}}>
        {(persons)
            ?<Table>
                <TableBody>
                  {Object.keys(persons).map((id) => {
                    return (
                      <TableRow key={id} onClick={() => this.props.editPerson(id)}>
                        {Object.keys(persons[id]).map(prop => ((prop!=='onTerritory')&&(prop!=='access'))?<TableCell key={prop} data={prop}>{persons[id][prop]}</TableCell>:null)}
                        <TableCell>
                          {/* <EditIcon onClick={() => this.props.editPerson(id)}/> */}
                          {/* <DeleteIcon onClick={() => this.deletePerson(id)}/> */}
                        </TableCell>
                      </TableRow>)
                    })
                  }
                </TableBody>
            </Table>
            :null
        }
      </Paper>
    );
  }

}

export default PersonList;
