import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import {FormControl} from 'material-ui/Form';
import Button from 'material-ui/Button';
import {InputLabel} from 'material-ui/Input';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import moment from 'moment';

class TsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      tsData:null
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if (nextProps.id!==prevState.id) {
      return {id:nextProps.id, tsData: null};
    };
    return null;
  }

  getTsData = (id) => {
      fetch('/ts/'+id)
        .then(response => response.json())
        .then(data => {
            this.setState({tsData:data});
        })
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.state.tsData === null)&&(this.state.id)) {
      this.getTsData(this.state.id)
    }
  }

  transferTs = (id) => {
    console.log(id);
    fetch('/tss/transfer/'+id)
      .then((res) => this.getTsData(id))
      .catch((res) => console.log(res))
  }

  inputChange = (field) => {
    this.setState({
      ...this.state,
      ...{tsData:
            {...this.state.tsData,
              ...{[field.name]: field.value}}}
          })
  }


  addTs = () => {
    let formData = {...this.state.tsData};
    formData.onTerritory = false;
    this.setState({tsData:null});
    fetch("/ts/create",
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(formData)
      })
      .then(function(res){ console.log(res) })
      .catch(function(res){ console.log(res) })
  }

  render() {
     let tsData = this.state.tsData;
    return (
      <Paper style={{margin:12,padding:32,display:'flex',flexDirection:'column'}}>
        <FormControl>
                <InputLabel>Наименование ТС</InputLabel>
                <Select name="type" value={(tsData&&tsData.type)?tsData.type:'car'} onChange={(node) => this.inputChange(node.target)}>
                  <MenuItem value={'car'}>Легковой</MenuItem>
                  <MenuItem value={'truck'}>Грузовой</MenuItem>
                  <MenuItem value={'special'}>Спец. техника</MenuItem>
                </Select>
        </FormControl>
        <TextField name='model' label='Модель' onChange={(node) => this.inputChange(node.target)} value={(tsData&&tsData.model)?tsData.model:''}/>
        <TextField name='licPlate' label='Гос. номер' onChange={(node) => this.inputChange(node.target)} value={(tsData&&tsData.licPlate)?tsData.licPlate.toUpperCase():''}/>
        <TextField type="number" name='location' label='Номер участка' onChange={(node) => this.inputChange(node.target)} value={(tsData&&tsData.location)?tsData.location:''}/>
        <FormControl>
                <InputLabel>Тип Доступа</InputLabel>
                <Select name="access" value={(tsData&&tsData.access)?tsData.access:'permanent'} onChange={(node) => this.inputChange(node.target)}>
                  <MenuItem value={'permanent'}>Постоянный</MenuItem>
                  <MenuItem value={'once'}>Разовый</MenuItem>
                  <MenuItem value={'temporal'}>Временный доступ</MenuItem>
                </Select>
        </FormControl>
        {(tsData&&(tsData.access==='temporal'))
          ?<TextField
              label="Действителен до"
              name='expires'
              type="date"
              value={(tsData&&tsData.expires)?moment(tsData.expires).format('YYYY-MM-DD'):moment().format('YYYY-MM-DD')}
              // values={(tsData&&tsData.expires)?moment(tsData.expires).toDate():''}
              // defaultValue={}
              onChange={(node) => this.inputChange(node.target)}
              InputLabelProps={{
                shrink: true,
              }}
            />
          :null}
        {(!this.state.id)
          ?<Button variant='raised' color="inherit" onClick={this.addTs}>Добавить ТС</Button>
          :<Button variant='raised' color="inherit" onClick={this.updateTs}>Изменить данные ТС</Button>}
      </Paper>
    );
  }

}

export default TsForm;
