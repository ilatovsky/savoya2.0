import React, { Fragment,Component } from 'react';
import moment from 'moment';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Card, { CardHeader,  CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Done from '@material-ui/icons/Done';
import Close from '@material-ui/icons/Close';
import ArrowBack from '@material-ui/icons/ArrowBack';
import ArrowForward from '@material-ui/icons/ArrowForward';
import ExpandMore from '@material-ui/icons/ExpandMore';
import EditIcon from '@material-ui/icons/Edit';

const checkAccess = ({accessType, expires = Date.now()}) => {
  switch (accessType) {
    case 'temporal':
      console.log();
      return (parseInt(moment(expires).format('x'),10)>Date.now())
    case 'closed':
      return false;
    default:
      return true;
  }
}

class PersonDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      personData:null
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if (nextProps.id!==prevState.id) {

      return {id:nextProps.id, personData: null};
    };
    return null;
  }

  getPersonData = (id) => {
      fetch('/persons/'+id)
        .then(response => response.json())
        .then(data => {
            this.setState({personData:data});
        })
  }

  componentDidUpdate(prevProps, prevState) {
    if ((this.state.personData === null)&&(this.state.id)) {
      this.getPersonData(this.state.id)
    }
  }

  transferPerson = (id) => {
    console.log(id);
    fetch('/persons/transfer/'+id)
      .then((res) => this.getPersonData(id))
      .catch((res) => console.log(res))
  }

  render() {
    let personData = this.state.personData;
    let access = (personData)?checkAccess({accessType:personData.access,expires:personData.expires}):false;
    return (
      // <Paper style={{margin:12,padding:32,display:'flex',flexDirection:'column'}}>
      <Fragment>
        {(personData)
          ?<Card style={{margin:12}}>
            <CardHeader
              avatar={
                (access)
                  ?<Avatar style={{backgroundColor:'#4CAF50'}} aria-label="Recipe">
                    <Done />
                  </Avatar>
                  :<Avatar style={{backgroundColor:'#C62828'}} aria-label="Recipe">
                    <Close />
                  </Avatar>}
            title={`${personData.last} ${personData.first} ${personData.father}`}
            subheader={personData.phone}
            // action={<EditIcon onClick={() => this.props.editPerson(this.state.id)}/>}
            >
            </CardHeader>
            <CardContent>
              <Typography variant="caption" gutterBottom>{`Номер участка: ${personData.location}`}</Typography>
              <Typography variant="caption" gutterBottom>{`Тип доступа: ${
                (
                  () => {
                    switch (personData.access) {
                      case 'permanent':
                        return 'постоянный';
                      case 'temporal':
                        return 'временный';
                      case 'once':
                        return 'разовый';
                      case 'closed':
                        return 'закрыт';
                      default:
                        return '';
                    }
                  }
                  )()
              }`}
            </Typography>
            {(personData.access==='temporal')?<Typography variant="caption" style={(access)?{color:'#4CAF50'}:{color:'#C62828'}} gutterBottom>{`Дата окончания доступа: ${personData.expires}`}</Typography>:null}
            </CardContent>
            <CardActions style={{display: 'flex',justifyContent: 'space-between'}}>
              {(access)
              ?(personData.onTerritory)
                ?<Button variant="raised" color="primary" onClick={() => this.transferPerson(this.state.id)}>
                  {'Фиксировать выход'}
                  <ArrowBack />
                </Button>
                :<Button variant="raised" color="primary" onClick={() => this.transferPerson(this.state.id)}>
                  <ArrowForward />
                  {'Фиксировать вход'}
                </Button>
              :<span></span>}
              <IconButton>
                <ExpandMore />
              </IconButton>
            </CardActions>
          </Card>
          :null}
      </Fragment>
    );
  }

}

export default PersonDetails;
