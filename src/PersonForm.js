import React, { Component } from 'react';
import moment from 'moment';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import {FormControl} from 'material-ui/Form';
import Input, {InputLabel} from 'material-ui/Input';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import MaskedInput from 'react-text-mask';

function TextMaskCustom(props) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={inputRef}
      mask={['+','7',' ','(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
      guide={false}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}


class PersonForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: null,
      personData:null
    };
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if (nextProps.id!==prevState.id) {
      return {id:nextProps.id, personData: null};
    };
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.id&&(this.state.personData === null)) {
      fetch('/persons/'+this.props.id)
        .then(response => response.json())
        .then(data => {
            this.setState({personData:data});
        })
    }
  }

  updatePerson = () => {
    let formData = {...this.state};
    console.log(formData);
    fetch("/persons/edit/"+this.state.id,
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(this.state.personData)
      })
      .then(function(res){ console.log(res) })
      .catch(function(res){ console.log(res) })
  }

  addPerson = () => {
    let formData = {...this.state.personData};
    formData.onTerritory = false;
    this.setState({personData:null});
    fetch("/persons/create",
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: "POST",
        body: JSON.stringify(formData)
      })
      .then(function(res){ console.log(res) })
      .catch(function(res){ console.log(res) })
  }


  inputChange = (field) => {
    this.setState({
      ...this.state,
      ...{personData:
            {...this.state.personData,
              ...{[field.name]: field.value}}}
          })
  }

  render() {
    let personData = this.state.personData;
    return (
      <Paper style={{margin:12,padding:32,display:'flex',flexDirection:'column'}}>
          <TextField name='last' label='Фамилия' onChange={(node) => this.inputChange(node.target)} value={(personData&&personData.last)?personData.last:''}/>
          <TextField name='first' label='Имя' onChange={(node) => this.inputChange(node.target)} value={(personData&&personData.first)?personData.first:''}/>
          <TextField name='father' label='Отчество' onChange={(node) => this.inputChange(node.target)} value={(personData&&personData.father)?personData.father:''}/>
          <FormControl>
            <InputLabel htmlFor="formatted-text-mask-input">Телефон</InputLabel>
            <Input name='phone'
              inputComponent={TextMaskCustom}
              onChange={(node) => this.inputChange(node.target)}
              value={(personData&&personData.phone)?personData.phone:''}
            />
          </FormControl>
          <TextField
            name='location'
            label='Номер участка'
            onChange={(node) => this.inputChange(node.target)}
            value={(personData&&personData.location)?personData.location:''}/>
          <FormControl>
                  <InputLabel>Тип Доступа</InputLabel>
                  <Select name="access" value={(personData&&personData.access)?personData.access:'permanent'} onChange={(node) => this.inputChange(node.target)}>
                    <MenuItem value={'permanent'}>Постоянный</MenuItem>
                    <MenuItem value={'once'}>Разовый</MenuItem>
                    <MenuItem value={'temporal'}>Временный доступ</MenuItem>
                  </Select>
          </FormControl>
          {(personData&&(personData.access==='temporal'))
            ?<TextField
                label="Действителен до"
                name='expires'
                type="date"
                value={(personData&&personData.expires)?moment(personData.expires).format('YYYY-MM-DD'):moment().format('YYYY-MM-DD')}
                // values={(personData&&personData.expires)?moment(personData.expires).toDate():''}
                // defaultValue={}
                onChange={(node) => this.inputChange(node.target)}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            :null}
          {(!this.state.id)
            ?<Button variant='raised' color="inherit" onClick={this.addPerson}>Добавить персону</Button>
            :<Button variant='raised' color="inherit" onClick={this.updatePerson}>Изменить данные</Button>}

      </Paper>
    );
  }

}

export default PersonForm;
